# Sunika - online sneaker store.

We are here - http://sunika.ff44bd.ru.

## Scripts

### `yarn start`

Runs the app in the development mode at http://localhost:3000.

### `yarn test`

Launches the test runner.

### `yarn lint`

Launches ESLint and automatically fix problems.

### `yarn build`

Builds the app for production to the `build` folder.

## Tech stack

* TypeScript
* React
* Redux
* ESLint
* Prettier